using System.Text.RegularExpressions;

namespace cezar
{
    public partial class Form1 : Form
    {
        private List<int> shiftValues;
        public Form1()
        {
            InitializeComponent();
            shiftValues = new List<int>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string plaintext = textBox1.Text;

            EncryptDecryptText(true);
            string encryptedText = textBox2.Text;

            DisplayShiftValues(plaintext, encryptedText);
        }

        private void EncryptDecryptText(bool encrypt)
        {
            string text = textBox1.Text.ToUpper();
            bool isValid = IsEnglishText(text);

            if (!isValid)
            {
                MessageBox.Show("����� ������ �������, �� �� � ����������� �������.", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit(); 
            }
            int shift = Convert.ToInt32(numericUpDown1.Value);
            string result = "";

            foreach (char c in text)
            {
                if (char.IsLetter(c))
                {
                    int alphabetIndex = c - 'E';
                    int shiftedIndex = encrypt ? (alphabetIndex + shift) % 26 : (alphabetIndex - shift + 26) % 26;
                    char shiftedChar = (char)('E' + shiftedIndex);
                    result += shiftedChar;
                }
                else
                {
                    result += c;
                }
            }

            textBox2.Text = result;
        }
        private void DisplayShiftValues(string inputText, string outputText)
        {
            dataGridView1.Rows.Clear();

            if (!string.IsNullOrEmpty(inputText) && !string.IsNullOrEmpty(outputText))
            {
                dataGridView1.ColumnCount = 4;

                int maxLength = Math.Min(inputText.Length, outputText.Length);

                dataGridView1.RowCount = maxLength;

                for (int i = 0; i < maxLength; i++)
                {
                    char currentChar = inputText[i];
                    char shiftedChar = outputText[i];

                    dataGridView1.Rows[i].Cells[0].Value = currentChar;
                    dataGridView1.Rows[i].Cells[1].Value = (int)currentChar - (char.IsUpper(currentChar) ? 65 : 97);
                    dataGridView1.Rows[i].Cells[2].Value = (int)shiftedChar - (char.IsUpper(shiftedChar) ? 65 : 97);
                    dataGridView1.Rows[i].Cells[3].Value = shiftedChar;
                }
            }
        }
        static bool IsEnglishText(string text)
        {
            Regex regex = new Regex("^[a-zA-Z]+$"); 
            return regex.IsMatch(text);
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            string plaintext = textBox1.Text;

            EncryptDecryptText(false);
            string encryptedText = textBox2.Text;

            DisplayShiftValues(plaintext, encryptedText);
        }
    }
}
